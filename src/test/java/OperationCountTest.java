import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runners.Parameterized;
import org.junit.runner.RunWith;
import org.junit.Before;
import org.junit.After;

@RunWith(Parameterized.class)
public class OperationCountTest extends Object {
	private String input;
	public static OperationCount count;
	private int expectedresultl;
	private int expectedresultw;
	private int expectedresultc;

	@Before
	public void initialize() {
		count = new OperationCount();
		
	}

	public OperationCountTest(String text, Integer expectedresultLine, Integer expectedC, Integer expectedw) {
		// TODO Auto-generated constructor stub
		this.input = text;
		this.expectedresultl = expectedresultLine;
		this.expectedresultc = expectedC;
		this.expectedresultw = expectedw;
	}

	@Parameterized.Parameters
	public static Collection LCount() {
		return Arrays.asList(new Object[][] { 
			{ "Hello Hi", 1, 7, 2 },
			{"This is software engineering",1,25,4},
			{"Hello Hi\n",1,8,2},
			
			{null,0,0,0}
		});
	}

	@Test
	public void testwordcount() {
		// fail("Not yet implemented");
		count.wordc(input);

		assertEquals(expectedresultw, count.wordcount);
		

	}

	@Test
	public void testLinecount() {
		// fail("Not yet implemented");
		count.linec(input);
		assertEquals(expectedresultl, count.linecount);
	}

	@Test
	public void testCharcount() {
		// fail("Not yet implemented");
		count.charc(input);

		assertEquals(expectedresultc, count.charcount);

	}
	@After
	public void destroy() {
		count = null;
	}
	
	

}
