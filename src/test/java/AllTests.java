import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.junit.runner.Result;
import org.junit.runner.JUnitCore;
import org.junit.runner.notification.Failure;

@RunWith(Suite.class)
@SuiteClasses({ OperationCountTest.class, OperationFrequencyTest.class })
public class AllTests {

	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(OperationCountTest.class, OperationFrequencyTest.class, OperationSearchTest.class );
		for (Failure failure : result.getFailures()) {
	         System.out.println(failure.toString());
	      }
			
	      System.out.println(result.wasSuccessful());
	   }
}


//, OperationSearchTest.class