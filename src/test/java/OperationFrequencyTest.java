import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import org.junit.Test;
import org.junit.runners.Parameterized;
import org.junit.runner.RunWith;
import org.junit.After;
import org.junit.Before;

@RunWith(Parameterized.class)
public class OperationFrequencyTest {
	private String input;
	
	private Integer freq;
	public static OperationFrequency frequency;
	HashMap<String, Integer> expectedresult;

	@Before
	public void initialize() {
		frequency = new OperationFrequency();
		expectedresult = new HashMap<String, Integer>();
	}

	public OperationFrequencyTest(String text, Integer num) {
		// TODO Auto-generated constructor stub
		//line = "yes hello hi yes";
		this.input = text;
		this.freq = num;
	}

	@Parameterized.Parameters
	public static Collection freqColl() {
		return Arrays.asList(new Object[][] { { "hi", 1 }, { "yes", 1 }, { "hello", 1 },

		});
	}

	@Test
	public void testWord() {
		// fail("Not yet implemented");
		// OperationFrequency freq = new OperationFrequency();
		String[] input = frequency.split("yes hello hi yes");
		frequency.freqcalc(input);
		//expectedresult.put(input, freq);
		assertEquals(2,(int)frequency.wordcount.get("yes"));
		assertEquals(null,frequency.wordcount.get("bye"));
		assertEquals(true,frequency.wordcount.containsKey("hello"));
		assertEquals(false,frequency.wordcount.containsKey("Bhatia"));
	}

	@After
	public void destroy() {
		frequency = null;
	}
}
