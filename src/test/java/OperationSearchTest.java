import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import org.junit.Test;
import org.junit.runners.Parameterized;
import org.junit.runner.RunWith;
import org.junit.After;
import org.junit.Before;

@RunWith(Parameterized.class)
public class OperationSearchTest {
	private String input;
	//private String line;
	public static OperationSearch searchword;
	public static OperationSearch get;
	private String expectedwordpresentresult;
	private String expectedwordabsentresult;
	private String word;
	private String getword;//change made
	private ByteArrayOutputStream outContent;

	@Before
	public void initialize() {
		outContent = new ByteArrayOutputStream();
		searchword = new OperationSearch(word);
		get = new OperationSearch(getword);//change made
		System.setOut(new PrintStream(outContent));
	}
	
	//change made : have added two more parameters for word not present situations
	public OperationSearchTest(String expectedwordpresentresult, String findword, String expectedwordabsentresult, String word) {
		// TODO Auto-generated constructor stub
		this.expectedwordpresentresult = expectedwordpresentresult;//change made
		this.word = findword;//change made
		this.expectedwordabsentresult = expectedwordabsentresult;//change made
		this.getword = word;//change made
		
	}

	@Parameterized.Parameters
	public static Collection WSearch() {
		return Arrays.asList(new Object[][] {
			{ "My greatest strength lies in my ability effectively to communicate", "communicate","my name is","isa"},//change made
			{"Pizza is yummy","yummy","The world has its end","devastation"},//change made
			{"I study at ULL","ULL","I live at USA","France"},//change made
		});
	}
	@Test
	public void wordpresenttest() throws Exception {
		searchword.doOperation(expectedwordpresentresult);
		assertEquals(expectedwordpresentresult, outContent.toString());
	}
	
	//change made : have defined test case for scenario when word is not found during search
	@Test
	public void wordabsenttest() throws Exception{
		get.doOperation(expectedwordabsentresult);
		assertNotEquals(expectedwordabsentresult, outContent.toString());
	}
	
	@After
	public void destroy() {
		outContent = null;
		searchword = null;
		get = null; //change made
	}
}

