
//package Try2;

public class Context {
	private Strategy strategy;

	   public Context(Strategy strategy){
	      this.strategy = strategy;
	   }

	   public void executeStrategy(String data){
		   strategy.doOperation(data);
	   }
	   public void output() {
		   strategy.printoutput();
	   }
}
