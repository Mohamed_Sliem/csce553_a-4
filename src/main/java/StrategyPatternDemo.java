import java.io.BufferedReader;

import java.io.FileReader;
import java.io.IOException;


public class StrategyPatternDemo {
	
	public static void main(String[] args) throws IOException {
		//int count = args.length;
		
		try {
			String operation = args[0];
			
			if(args.length == 2) {
				String file = args[1];
				
				if(operation.equalsIgnoreCase("wc")) 
				{
					Context context = new Context(new OperationCount());
					FileRead(file, context);
					context.output();
				}
				else if(operation.equalsIgnoreCase("freq"))
				{
					Context context = new Context(new OperationFrequency());
					FileRead(file, context);
					context.output();
				}
			}
			else 
			{
				String word = args[1];
				String file = args[2];
				Context context = new Context(new OperationSearch(word));
				FileRead(file, context);
				//context.output();
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}	
	}
	public static String FileRead(String file, Context context) {
		 String data = "";
		 String pathName = "C:\\coderepository\\A3\\src\\main\\resources\\";
		 try {
			 BufferedReader reader = new BufferedReader(new FileReader(pathName+file+".txt")); //.txt pathName+
			 String line = reader.readLine();
			 while(line != null) {
				 data = line;
				 context.executeStrategy(data);
				 line = reader.readLine();
			 }
			 reader.close();
		 }
		 catch(Exception e) {
			 e.printStackTrace();
		 }
		 return null;
	}

}

 
 