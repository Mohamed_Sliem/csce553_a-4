
public class OperationCount implements Strategy{
	 int charcount;
	 int wordcount;
	 int linecount;
	   
	 OperationCount(){
		   charcount = 0;
		   wordcount = 0;
		   linecount = 0;
		   
	   }
	
	 public void doOperation(String data){
		 String d = data;
		 linec(d);
		 wordc(d);
		 charc(d);
		 
		 
	 }
	 
	 public void linec(String d) {
		 try {
			   if(d != null) {
				   linecount++;
			   }
		 }
		 catch(Exception e) {
			   e.printStackTrace();
		   }
	 }
	 public void wordc(String d) {
		 try {
			   if(d != null) {
				   String[] words = d.split(" ");
				   wordcount += words.length;
			   }
		 }
		 catch(Exception e) {
			   e.printStackTrace();
		   }
	 }
	 public void charc(String d) {
		 try {
			   if(d != null) {
				   String[] words = d.split(" ");
				   for (String indword :words) {
					   charcount += indword.length();
				   }
			   }
		 }
		 catch(Exception e) {
			   e.printStackTrace();
		   }
	 }
	public void printoutput() {
			     System.out.println("Number Of Chars : "+charcount);
			     System.out.println("Number Of Words : "+wordcount);
			     System.out.println("Number Of Lines : "+linecount);
	}
}		   
